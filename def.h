#ifndef DEF_H
#define DEF_H

#include <cmath>

#define K   500     //мах к-ть ітерацій
#define N   20      //початкова кількість клітин 
#define NC  10      //к-ть клонів для кожної клітини
#define d   0.4     //коефіцієнт клітин з найгіршим приспособленням які будуть замінені
#define b   100     //параметр операції клонування
#define r    0.2    //параметр мутації
#define unfit -9223372036854775808.0  
#define delta 0.00001


//#define FUNCTION(x, y) 4*sin(2*M_PI*x)*cos(1.5*M_PI*y)*(1-pow(x,2.0))*y*(1-y)




#endif