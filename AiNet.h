#ifndef AINET_H
#define AINET_H


#include <vector>
#include <cmath>
#include "def.h"
#include "cell.h"
#include "source.h"
#include <iostream>
#define _USE_MATH_DEFINES

class AiNet{
public:
    AiNet(double, double, double, double);
    void new_generate(double, double, double, double);
    cell* max();
    double value(int i){
        return function(population[i].get_x1(), population[i].get_x2());
    }

    //Derivative
    void der(int i){
        cell x = population[i];
        cout <<"dx ";
        cout << (function(x.get_x1() + delta, x.get_x2()) - function(x.get_x1(), x.get_x2()))/delta<<endl;
        cout <<"dy ";
        cout << (function(x.get_x1(), x.get_x2() + delta) - function(x.get_x1(), x.get_x2()))/delta<<endl;
    }
private:
    //к-ть клітин в популяції
    int num_population = N;
    //к-ть клонів для кожної клітини
    int num_clone = NC;

    
    
    double x1;
    double x2;
    double y1;
    double y2;

    //середнє значення популяції
    double average_population = unfit;
    double copy_average_population = unfit;

    //значення фітнес функції в population[]
    vector<double> fit_value;

    //значення фітнес функції в clone[]
    vector<std::vector<double>> fit_value_clone;

    //нормоване значення фітнес функції в population[]
    vector<double> n_fit_value;
    
    //ФІТНЕС ФУНКЦІЯ
    double function (double, double);
    
    //сторення клонів для даної популяції
    void clone();
    
    //отримати вектор значеннь фітнес функції для клітин даної популяції
    void popVal();
    
    //отримати нормований вектор значень фітнес функції для даної популяції
    void normPopVal();
    
    //отримати матрицю значень фітнес функції для клонів даної популяції
    void cloneVal();

    //мутація клонів
    void mutation_clone();

    //сортуємо популяцію в порядку зростання значення фітнес функції    //TODO переробить на інший алгоритм сортування
    void sort();

    //вибір кращих представників серед клонів і популяції
    void next_generate();

    //зробити копію терперішньої популяції
    void copy();

    //average utility of the population
    //середня корисність популяції
    void average();

    //
    void restart();

    //стиснення плпуляції
    void contraction();

    //поаертає евклідову норму між вказаними елементами
    double norm(int, int);

    void random_generate();


    //вказівник на першого члена популяції
    cell *population;
    //копія теперішньої популяції
    cell *copy_population;
    //вказівник на перший індекс матриці клонів
    cell **p_clone;

};

//в конструктор подаєм значчення проміжків на яких шукаємо max
AiNet::AiNet( double x1, double x2, double y1, double y2)
{
    this->new_generate(x1, x2, y1, y2);
}

void AiNet::new_generate(double x1, double x2, double y1, double y2)
{
    this->x1 = x1;
    this->x2 = x2;
    this->y1 = y1;
    this->y2 = y2;

    //ініціалізація масивів
    fit_value.resize(num_population);
    n_fit_value.resize(num_population);
    fit_value_clone.resize(num_population);
    for (int i = 0; i < num_population; i++)
        fit_value_clone[i].resize(num_clone);

    population =  new cell[num_population];
    copy_population = new cell[num_population];

    //створення нового покоління
    srandom(time(NULL));
    for (int i = 0; i < num_population; i++)
    {
        this->population[i].set(un_random(x1, x2), un_random(y1, y2));
    }
    this->copy();
    this->average();
    copy_average_population = average_population;

    //ініціалізація двовимірного масиву для клонів
    this->p_clone = new cell*[num_population];
    for (int i = 0; i < num_population; i++)
        p_clone[i] = new cell[num_clone];
}



double AiNet::function(double x, double y)
{
    return (pow(x, 2.0) + pow(y,2.0) - 10*( cos(2*M_PI*x) + cos(2*M_PI*y)));
}

void AiNet::clone()
{
    for (int j = 0; j < num_population; j++)
    {
        for (int i = 0; i < num_clone; i++)
        {
            p_clone[j][i].set(population[j].get_x1(), population[j].get_x2());
        }
    }
}

void AiNet::popVal()
{
    for (int i = 0; i < num_population; i++)
        this->fit_value[i] = function(population[i].get_x1(), population[i].get_x2());
}

void AiNet::normPopVal()
{
    //шукаємо коефіцієнт нормування
    double norm=0;
    for (int i = 0; i < num_population; i++)
        norm += pow(fit_value[i], (double)2);
    norm = sqrt(norm);

    //нормуємо значення в точці
    for (int i = 0; i < num_population; i++)
        n_fit_value[i] = fit_value[i]/norm;

    double min = n_fit_value[0];
    double max = n_fit_value[0];
    for (int i = 1; i < num_population; i++){
        if (n_fit_value[i] < min) min = n_fit_value[i];
        if (n_fit_value[i] > max) max = n_fit_value[i];
    };

   //приводимо до проміжку [0;1]
    if(min < 0)
    {
        max += fabs(min);
        for (int i = 0; i < num_population; i++)
        {
            n_fit_value[i] += fabs(min);
            n_fit_value[i] /= max;
        }
    }
}

void AiNet::cloneVal()
{
    for (int i = 0; i < num_population; i++)
    {
        for(int j = 0; j < num_clone; j++)
        {
            fit_value_clone[i][j] = function(p_clone[i][j].get_x1(), p_clone[i][j].get_x2());
        }
    }
}


void AiNet::mutation_clone()
{
    for (int i = 0; i < num_population; i++)
    {
        for (int j = 0; j < num_clone; j++)
            p_clone[i][j].mutate(n_fit_value[i], this->x1, this->x2, this->y1, this->y2);
    }
}


void AiNet::next_generate()
{
    this->cloneVal();
    //шукаємо представників з найбільшим значенням фітнес функції
    for (int i = 0; i < num_population; i++)
    {
        for (int j = 0; j < num_clone; j++)
        {
            if(fit_value[i] < fit_value_clone[i][j])
            {
                fit_value[i] = fit_value_clone[i][j];
                population[i] = p_clone[i][j];
            }
        }
    }
}


void AiNet::sort()
{
    for (int i = 0; i < num_population; i++)
        for (int j =  i+1; j < num_population; j++)
            if( fit_value[j] < fit_value[i]){
                swap(fit_value[j], fit_value[i]);
                swap(population[i], population[j]);
                swap(p_clone[i], p_clone[j]);
            }
}


void AiNet::copy()
{
    for(int i = 0; i < num_population; i++)
        copy_population[i] = population[i];
    this->average();
    copy_average_population = average_population;

}

void AiNet::average()
{
    this->popVal();
    average_population = 0;
    for (int i = 0 ; i < num_population; i++)
        average_population += fit_value[i];
    average_population /= num_population;
}


void AiNet::restart()
{
    //відновлюємо попереднє покоління
    for(int i = 0; i < num_population; i++)
        population[i] = copy_population[i];
    this->average();

}

double AiNet::norm(int i, int j)
{
    return sqrt(pow(population[i].get_x1() - population[j].get_x1(), 2.0) + pow(population[i].get_x2() - population[j].get_x2(), 2.0));
}

void AiNet::contraction()
{
    for (int i = num_population -1; i >= 0; i--)
    {
        if (fit_value[i] == unfit) continue;
        for (int j = i - 1; j >= 0; j--)
        {
            if (fit_value[j] == unfit) continue;
            if ( this->norm(i,j) < r)
            {
                if ( fit_value[i] >= fit_value[j]) fit_value[j] = unfit;
                else 
                {
                    fit_value[i] = unfit;
                    break;
                }
            }
        }
    }
}


void AiNet::random_generate()
{
    for (int i = 0; i < int(d*num_population); i++)
        population[i].set(un_random(x1, x2), un_random(y1, y2));
    for(int i = int(d*num_population); i < num_population; i++)
        if(fit_value[i] == unfit) population[i].set(un_random(x1, x2), un_random(y1, y2));
}


cell* AiNet::max()
{
    for (int i = 0; i < K; i++)
    {
        this->popVal();
        this->normPopVal();
        this->clone();
        this->mutation_clone();
        this->cloneVal();
        this->next_generate();
        this->average();
        this->sort();
        if(average_population < copy_average_population)
        {
            this->restart();
            i--;
        }
        else
        {
            this->sort();
            this->contraction();
            this->sort();
            this->random_generate();
            this->copy();
        }
    }
    this->popVal();
    this->sort();
    for (int i = num_population - 1; i >7; i--){
        population[i].out(); cout<<endl;}
    return population;
}


#endif